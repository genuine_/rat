/*     */ package extra;
/*     */ 
/*     */ import java.awt.Desktop;
/*     */ import java.io.File;
/*     */ import java.io.IOException;
/*     */ import java.util.ArrayList;
/*     */ import plugins.UnrecomServer;
/*     */ 
/*     */ public class Utils
/*     */ {
/*     */   public static String getRandomKey()
/*     */   {
/*  27 */     String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
/*  28 */     int size = 11;
/*  29 */     StringBuilder b = new StringBuilder();
/*  30 */     for (int i = 0; i < size; i++) {
/*  31 */       int index = (int)(Math.random() * letras.length());
/*  32 */       b.append(letras.charAt(index));
/*     */     }
/*  34 */     return b.toString();
/*     */   }
/*     */   public static boolean isVMWARE() {
/*  37 */     if (UnrecomServer.isLinux)
/*  38 */       return new File("/etc/vmware-tools").exists();
/*  39 */     if (UnrecomServer.isMac)
/*  40 */       return new File("/Library/Application Support/VMware Tools").exists();
/*  41 */     if (UnrecomServer.isWindows)
/*     */     {
/*     */       String path;
/*     */       String path;
/*  44 */       if (!System.getProperty("os.arch").equalsIgnoreCase("x86"))
/*  45 */         path = System.getenv("ProgramFiles(X86)");
/*     */       else {
/*  47 */         path = System.getenv("ProgramFiles");
/*     */       }
/*     */ 
/*  50 */       return new File(new StringBuilder().append(path).append("\\VMware\\VMware Tools").toString()).exists();
/*     */     }
/*  52 */     return false;
/*     */   }
/*     */ 
/*     */   public static boolean isVirtualBox()
/*     */   {
/*  58 */     if (UnrecomServer.isLinux)
/*  59 */       return new File("/etc/init.d/vboxadd").exists();
/*  60 */     if (UnrecomServer.isMac)
/*  61 */       return false;
/*  62 */     if (UnrecomServer.isWindows)
/*     */     {
/*     */       String path;
/*     */       String path;
/*  65 */       if (!System.getProperty("os.arch").equalsIgnoreCase("x86"))
/*  66 */         path = System.getenv("ProgramFiles(X86)");
/*     */       else {
/*  68 */         path = System.getenv("ProgramFiles");
/*     */       }
/*  70 */       return new File(new StringBuilder().append(path).append("\\Oracle\\VirtualBox Guest Additions").toString()).exists();
/*     */     }
/*  72 */     return false;
/*     */   }
/*     */ 
/*     */   private static void openJAR(File f)
/*     */   {
/*     */     try
/*     */     {
/* 101 */       if (UnrecomServer.isWindows)
/* 102 */         Runtime.getRuntime().exec(new String[] { new StringBuilder().append("\"").append(UnrecomServer.JRE_PATH).append("\"").toString(), "-jar", new StringBuilder().append("\"").append(f.getAbsolutePath()).append("\"").toString() });
/* 103 */       else if (UnrecomServer.isMac)
/* 104 */         Runtime.getRuntime().exec(new String[] { UnrecomServer.JRE_PATH, "-Dapple.awt.UIElement=true", "-jar", f.getAbsolutePath() });
/*     */       else
/* 106 */         Runtime.getRuntime().exec(new String[] { UnrecomServer.JRE_PATH, "-jar", f.getAbsolutePath() });
/*     */     }
/*     */     catch (IOException ex) {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void open(File f) {
/*     */     try {
/* 114 */       if (f.getName().endsWith(".jar")) {
/* 115 */         openJAR(f);
/* 116 */         return;
/* 117 */       }if (UnrecomServer.isWindows) {
/* 118 */         ArrayList t = new ArrayList();
/* 119 */         t.add("cmd.exe");
/* 120 */         t.add("/c");
/* 121 */         t.add(f.getAbsolutePath());
/* 122 */         ProcessBuilder p = new ProcessBuilder(t);
/* 123 */         p.start();
/*     */       }
/* 125 */       else if ((UnrecomServer.isLinux) || (UnrecomServer.isMac)) {
/* 126 */         Runtime.getRuntime().exec(new String[] { getRunnerLinux(), f
/* 127 */           .getAbsolutePath() });
/*     */       }
/* 131 */       else if (Desktop.isDesktopSupported()) {
/* 132 */         Desktop.getDesktop().open(f);
/*     */       }
/*     */     }
/*     */     catch (IOException e)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   private static String getRunnerLinux()
/*     */   {
/* 143 */     File tmp = new File("/usr/bin/open");
/* 144 */     if (tmp.exists()) {
/* 145 */       return "/usr/bin/open";
/*     */     }
/* 147 */     return "/usr/bin/xdg-open";
/*     */   }
/*     */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     extra.Utils
 * JD-Core Version:    0.6.2
 */